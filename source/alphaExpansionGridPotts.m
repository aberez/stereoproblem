function [labels, energy, time] = alphaExpansionGridPotts(unary, vertC, horC, metric, varargin)

%%������� alphaExpansionGridPotts
%����
%unary � ������� ����������, ������ ���� double ������� N x M x K, ��� N � ������ �������, M � ������ �������, K � ���������� �����.
%vertC � ������������ , ��������������� ������������ ������, ������ ���� double ������� (N � 1) x M;
%horC � ������������ , ��������������� �������������� ������, ������ ���� double ������� N x (M � 1);
%metric - ���������� ����� ������� �������� ����������, ������ ���� double ������� K x K;
%options � (�������������� ��������) ����� �������������� ����������, ��������� � ������
%  'maxIter' � ����������� ���������� ����� �������� ��������� (�� ��������� = 500);
%  'display' � �������� ���� logical: ���� true, �� ��� ������ ������� ��������� ������� ����� ����� �������� �� ����� ����� ��������, ������ �������������� �����, ������� �������� �������;
%  'numStart' � ���������� �������� �� ������ ��������� �����������;
%�����
%labels � ��������, ���������� ���������� ��������, ������ ���� double ������� N x M;
%energy � �������� ������� �� ������ ��������, ������ ���� double �����, ������ ���������� �������� ���������;
%time � �����, ���������� � ������ ������ ��������� �� ������ ��������, ������ ���� double �����, ������ ���������� �������� ���������;

%%������ �������������� ��������
p=inputParser;
default_maxIter=50;
default_display=true;
default_numStart=1;

addParamValue(p,'maxIter',default_maxIter);
addParamValue(p,'display',default_display);
addParamValue(p,'numStart',default_numStart);

parse(p,varargin{:});

maxIter=p.Results.maxIter;
display=p.Results.display;
numStart=p.Results.numStart;

N=size(unary,1);
M=size(unary,2);
K=size(unary,3);
%��������� ����.

%infim=10^9;%����� ����� ������ INF
infim=Inf;%������ ��� ����� :)
c_0=zeros(N,M);
c_1=zeros(N,M);
Eh00=zeros(size(horC));
Eh01=zeros(size(horC));
Eh10=zeros(size(horC));
Eh11=zeros(size(horC));
Ev00=zeros(size(vertC));
Ev01=zeros(size(vertC));
Ev10=zeros(size(vertC));
Ev11=zeros(size(vertC));
start_cut_best=Inf;
  
%%������� � �������� ��������� � ������������ ������������ �������
%����
% 1 4 7
% 2 5 8
% 3 6 9            
%������������� � �����������
vertex_index=reshape((1:N*M)',N,M);
vertexL=vertex_index(:,1:end-1);
vertexR=vertex_index(:,2:end);
vertexU=vertex_index(1:end-1,:);
vertexD=vertex_index(2:end,:);
            

%��������� ��������
for start_index=1:numStart
    %��������� ������
    tic;
    labels=randi(K,N,M);
    %labels=labels_init;
    iter_cut_best=Inf;
    alpha_cut_best=Inf;
    energy=[];
    time=[];
   
    for iter=1:maxIter
        %�������� �� ���� ������
        for alpha_label=1:K
            %���� �� ������������
            % terminalWeights=zeros(M*N, 2);
            % edgeWeights=zeros(M*(N-1)+N(M-1),4);
            
            %������ ��������������� ������� ������ �������, � �� ������ �����
            %���� ����������
            for y=1:N
                for x=1:M
                    %������� ��������������
                    if labels(y,x)==alpha_label
                         c_1(y,x)=infim;
                    else
                       c_1(y,x)= unary(y,x,labels(y,x));
                    end
                  
                    c_0(y,x)=unary(y,x,alpha_label);
                    %�������������
                    if x<M
                        Eh00(y, x) =horC (y,x)* metric(labels(y,x), labels(y, x+1)) ;
                        Eh01(y,x) = horC (y,x)* metric(labels(y,x), alpha_label);
                        Eh10(y,x) = horC (y,x)* metric(alpha_label, labels(y, x + 1));
                        Eh11(y,x) = horC (y,x)* metric(alpha_label, alpha_label);
                    end
                    %�����������
                    if y<N
                        Ev00(y, x) =vertC (y,x)* metric(labels(y,x), labels(y+1,x)) ;
                        Ev01(y,x) = vertC (y,x)* metric(labels(y,x), alpha_label);
                        Ev10(y,x) = vertC (y,x)* metric(alpha_label, labels(y+1, x));
                        Ev11(y,x) = vertC (y,x)* metric(alpha_label, alpha_label);
                    end
                end
            end
            
            %�������� ���� ������ ��� ������������ �� ��������, � ��
            %����� ���� ����������           
           
            
            %Ev01U=[Ev01; zeros(1,M)]; 
            %Ev01D=[ zeros(1,M); Ev01];
             
            %Eh01L=[Eh01 zeros(N,1)];
            %Eh01R=[zeros(N,1) Eh01];    
            
            Ev10U=[Ev10; zeros(1,M)]; 
            Ev10D=[zeros(1,M);Ev10];
            
            Eh10L=[Eh10 zeros(N,1)];
            Eh10R=[ zeros(N,1)  Eh10]; 
            
            Eh00R=[zeros(N,1), Eh00];
            Eh00L=circshift(Eh00R,[0 -1]);
            %Ev00D=[zeros(1,M); Ev00]; 
            Ev00U=[Ev00;zeros(1,M)];
            
            Ev11D=[zeros(1,M); Ev11];
            %Ev11U=[Ev11; zeros(1,M)];
            %Eh11L=[Eh11 zeros(N, 1)];            
            Eh11R=[zeros(N,1), Eh11];
            
            
            %������ �������
            c_1=c_1+Eh00L+Ev00U+Eh10R+Ev10D-Eh10L-Ev10U;
            c_0=c_0+Ev11D+Eh11R;
      
            % ����� �� ���� ����� �������� � ��������
            terminalWeights=[reshape(c_0, N*M, 1), reshape(c_1, N*M, 1)];          
            
            c_RL=Eh01+Eh10-Eh00-Eh11;
            c_DU=Ev01+Ev10-Ev00-Ev11;
            c_RL=round(c_RL);%
            c_DU=round(c_DU);
            
            edgeWeights1=[reshape(vertexL,N*(M-1),1) reshape(vertexR,N*(M-1),1)  reshape(c_RL,N*(M-1),1) zeros(N*(M-1),1)];
            edgeWeights2=[reshape(vertexU,(N-1)*M,1) reshape(vertexD,(N-1)*M,1)  reshape(c_DU,(N-1)*M,1) zeros((N-1)*M,1)];
            edgeWeights=[edgeWeights1;edgeWeights2];
              [cut, labels_alpha] = graphCutMex(terminalWeights,edgeWeights);
            energy=[energy; cut];
            time = [time, toc];
            if display
                s=sprintf('iter=%d \t alpha_label=%d \t energy=%d \t time=%5.2f', iter,alpha_label ,cut,toc);
                disp(s);
            end
            
            if (alpha_cut_best>cut)
                alpha_cut_best=cut;
                labels_alpha=reshape(labels_alpha, N,M);
                labels(labels_alpha==1)=alpha_label;                
                %outimage=imadjust(uint8(labels));
                %imwrite(outimage,sprintf('image_%d_%d.png%', iter,alpha_label ),'png')
            end
          
        end
        
        %�������� �� ��������� �������
        if iter_cut_best>alpha_cut_best 
           iter_cut_best=alpha_cut_best;
        else
            break;
        end
    end
    
    if start_cut_best>iter_cut_best
    start_labels_best=labels;
    start_energy_best=energy;
    start_time_best=time;
    end
end
   labels=start_labels_best;
   energy= start_energy_best;
   time= start_time_best;
end