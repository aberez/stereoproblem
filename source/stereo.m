function [disparity] = stereo(name)%, Lin)
%name - �������� ����������, �������� tsukuba
%����� tsukuba1.png - ������ ���� ����� ������������, tsukuba1.png  - ������
switch name
    case 'tsukuba'
        
        MAX_SHIFT=30; %������������ ���������� �������� �� ������� ����� ��������� ������ �������� ������������ �����.
        STEP_SHIFT=1; %���, �� ������� ����� ������������� ��������%����� �� ��������������
        L=5;
        RGB_image_left=imread(strcat(name,'1.png'));
        %imshow(RGB_image_left);
        Luv_image_left=RGB2Luv(RGB_image_left);
        %imshow(uint8(Luv_image_left(:,:,1)));
        RGB_image_right=imread(strcat(name,'2.png'));
        %imshow(RGB_image_right);
        Luv_image_right=RGB2Luv(RGB_image_right);
        
        %%������ ������� ����������
        unary_pot=zeros(size(RGB_image_left,1),size(RGB_image_left,2),MAX_SHIFT+1);
        for shift_right=0:MAX_SHIFT
            Luv_image_right_shifted=circshift(Luv_image_right,[0 shift_right]);
            
            %������ ������� ���������� ��� ����� shift_right ����� �������
            %(��������, ���������)
            unary_pot(:,:,shift_right+1)=sqrt((...
                (Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1)).*(Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1))+...
                (Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2)).*(Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2))+...
                (Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3)).*(Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3))...
                )/3);
            
            unary_pot(:,1:shift_right,shift_right+1)=deal(100);
            %imshow(uint8(unary_pot(:,:,shift_right+1)));
            %�������� 60 ������������ �������� �������� ����������
        end
        
        %%������ �������
        tmp_hor=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1);
        tmp_ver=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1)';
        D=abs(tmp_hor-tmp_ver).*(abs(tmp_hor-tmp_ver)<L)+L.*(abs(tmp_hor-tmp_ver)>=L);
        
        
        %%������ ������������ � �������������� �����
        vertC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[1 0]));
        vertC(end,:)=[];
        vertC=60./(10+vertC);
        %����� ��� ����� �� ������ - ���� �������� <8 �������� ��� ���� �� ���
        %vertC=(vertC<9)+1;
        %imshow(uint8(vertC));
        horC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[0 1]));
        horC(:,end)=[];
        %horC=(horC<9)+1;
        horC=60./(10+horC);
        %imshow(uint8(horC));
    case  'teddy'
        MAX_SHIFT=64; 
        STEP_SHIFT=1; 
        L=5;
        RGB_image_left=imread(strcat(name,'1.png'));
        Luv_image_left=RGB2Luv(RGB_image_left);
        RGB_image_right=imread(strcat(name,'2.png'));
        Luv_image_right=RGB2Luv(RGB_image_right);
        
        %%������ ������� ����������
        unary_pot=zeros(size(RGB_image_left,1),size(RGB_image_left,2),MAX_SHIFT+1);
        for shift_right=0:MAX_SHIFT
            Luv_image_right_shifted=circshift(Luv_image_right,[0 shift_right]);
            
            unary_pot(:,:,shift_right+1)=sqrt((...
                (Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1)).*(Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1))+...
                (Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2)).*(Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2))+...
                (Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3)).*(Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3))...
                )/3);
             unary_pot(:,1:shift_right,shift_right+1)=deal(100);
          
        end
        
        %%������ �������
        tmp_hor=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1);
        tmp_ver=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1)';
        D=abs(tmp_hor-tmp_ver).*(abs(tmp_hor-tmp_ver)<L)+L.*(abs(tmp_hor-tmp_ver)>=L);
        
        
        %%������ ������������ � �������������� �����
        vertC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[1 0]));
        vertC(end,:)=[];
        vertC=60./(40+vertC);
        %vertC=(vertC<9)*10+1;
        horC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[0 1]));
        horC(:,end)=[];
        %horC=(horC<9)*10+1;
        horC=60./(40+horC);  
    case 'art'
        MAX_SHIFT=80; 
        STEP_SHIFT=1; 
        L=5;
        RGB_image_left=imread(strcat(name,'1.png'));
        Luv_image_left=RGB2Luv(RGB_image_left);
        RGB_image_right=imread(strcat(name,'2.png'));
        Luv_image_right=RGB2Luv(RGB_image_right);
        
        %%������ ������� ����������
        unary_pot=zeros(size(RGB_image_left,1),size(RGB_image_left,2),MAX_SHIFT+1);
        for shift_right=0:MAX_SHIFT
            Luv_image_right_shifted=circshift(Luv_image_right,[0 shift_right]);
            
            unary_pot(:,:,shift_right+1)=sqrt((...
                (Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1)).*(Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1))+...
                (Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2)).*(Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2))+...
                (Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3)).*(Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3))...
                )/3);
            unary_pot(:,1:shift_right,shift_right+1)=deal(100);
            
        end
        
        %%������ �������
        tmp_hor=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1);
        tmp_ver=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1)';
        D=abs(tmp_hor-tmp_ver).*(abs(tmp_hor-tmp_ver)<L)+L.*(abs(tmp_hor-tmp_ver)>=L);
        
        
        %%������ ������������ � �������������� �����
        vertC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[1 0]));
        vertC(end,:)=[];
        vertC=50./(10+vertC);
        %vertC=(vertC<9)+1;
        horC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[0 1]));
        horC(:,end)=[];
        %horC=(horC<9)+1;
        horC=50./(10+horC); 
        
        case 'cones'
        MAX_SHIFT=65; 
        STEP_SHIFT=1; 
        L=10;
        RGB_image_left=imread(strcat(name,'1.png'));
        Luv_image_left=RGB2Luv(RGB_image_left);
        RGB_image_right=imread(strcat(name,'2.png'));
        Luv_image_right=RGB2Luv(RGB_image_right);
        
        %%������ ������� ����������
        unary_pot=zeros(size(RGB_image_left,1),size(RGB_image_left,2),MAX_SHIFT+1);
        for shift_right=0:MAX_SHIFT
            Luv_image_right_shifted=circshift(Luv_image_right,[0 shift_right]);
            
            unary_pot(:,:,shift_right+1)=sqrt((...
                (Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1)).*(Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1))+...
                (Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2)).*(Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2))+...
                (Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3)).*(Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3))...
                )/3);
            unary_pot(:,1:shift_right,shift_right+1)=deal(100);
            
        end
        
        %%������ �������
        tmp_hor=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1);
        tmp_ver=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1)';
        D=abs(tmp_hor-tmp_ver).*(abs(tmp_hor-tmp_ver)<L)+L.*(abs(tmp_hor-tmp_ver)>=L);
        
        
        %%������ ������������ � �������������� �����
        vertC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[1 0]));
        vertC(end,:)=[];
        vertC=60./(40+vertC);
        %vertC=(vertC<9)+1;
        horC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[0 1]));
        horC(:,end)=[];
        %horC=(horC<9)+1;
        horC=60./(40+horC); 
    otherwise
        MAX_SHIFT=63; 
        STEP_SHIFT=1; 
        L=5;
        RGB_image_left=imread(strcat(name,'1.png'));
        Luv_image_left=RGB2Luv(RGB_image_left);
        RGB_image_right=imread(strcat(name,'2.png'));
        Luv_image_right=RGB2Luv(RGB_image_right);
        
        %%������ ������� ����������
        unary_pot=zeros(size(RGB_image_left,1),size(RGB_image_left,2),MAX_SHIFT+1);
        for shift_right=0:MAX_SHIFT
            Luv_image_right_shifted=circshift(Luv_image_right,[0 shift_right]);
            
            unary_pot(:,:,shift_right+1)=sqrt((...
                (Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1)).*(Luv_image_right_shifted(:,:,1)-Luv_image_left(:,:,1))+...
                (Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2)).*(Luv_image_right_shifted(:,:,2)-Luv_image_left(:,:,2))+...
                (Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3)).*(Luv_image_right_shifted(:,:,3)-Luv_image_left(:,:,3))...
                )/3);
        end
        
        %%������ �������
        tmp_hor=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1);
        tmp_ver=repmat(0:MAX_SHIFT,MAX_SHIFT+1,1)';
        D=abs(tmp_hor-tmp_ver).*(abs(tmp_hor-tmp_ver)<L)+L.*(abs(tmp_hor-tmp_ver)>=L);
        
        
        %%������ ������������ � �������������� �����
        vertC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[1 0]));
        vertC(end,:)=[];
        vertC=60./(20+vertC);
        
        horC=abs(Luv_image_left(:,:,1)-circshift(Luv_image_left(:,:,1),[0 1]));
        horC(:,end)=[];

        horC=60./(20+horC); 

end

    
%  [labels2, energy2, time2] = alphaBetaSwapGridPotts(unary_pot, vertC, horC, D);
%  outimage2=imadjust(uint8(labels2));
% imshow(outimage2);
%  imwrite(outimage2,sprintf('teddy_alpha_beta.png'),'png');
%    



[labels, energy, time] = alphaExpansionGridPotts(unary_pot, vertC, horC, D);
% [labels1, energy1, time1] = alphaExpansionGridPotts(unary_pot, vertC, horC, D);
% outimage1=imadjust(uint8(labels1));
% imshow(outimage1);
%  %imwrite(outimage,sprintf('image_%d_e1010.png%', Lin),'png')
%  imwrite(outimage1,sprintf('teddy_alpha.png'),'png');
 disparity=labels; 

end