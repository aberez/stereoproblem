#Stereo problem#
This algorithm recovers shape (depths map) from stereo pair of images.
![shape.png](https://bitbucket.org/repo/4AEnpM/images/1943286548-shape.png)

Special Markov random field (MRF) is used to create a graphical model of scene. The problem reduces to the minimization of energy on MRF. To solve the energy minimization problem alpha-expansion algorithm with Graph Cuts is used. 